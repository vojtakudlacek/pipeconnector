from Graphics import Graphics
import Constants

import yaml

import os
from scipy import misc

class Core:
    def __init__(self, width, height, blocksize):
        self.graphics = Graphics(width, height, blocksize, Constants.PIPESIZE)
        self.loadMap("./testMap.yaml")
        self.drawMap()
        self.graphics.clickHandler();

    def loadMap(self, mapFile):
        '''
        Map is map of pipes object
        '''
        with open(mapFile,'r') as m:
            map = yaml.load(m)
        self.map = map['map']
        self.loadTextures()

    def loadTextures(self):
        path = Constants.PATHTOTEXTURES
        for i in range(len(self.map)):
            for j in range(len(self.map[i])):
                image = misc.imread(os.path.join(path, str(self.map[i][j]) + ".png"), flatten= 0)
                newImage = []
                for k in range(len(image)):
                    tmp = []
                    for l in range(len(image[k])):
                        tmp.append((image[k][l][0], image[k][l][0], image[k][l][0]))
                    newImage.append(tmp)
                self.map[i][j] = newImage #to tuple arraye

    def drawMap(self):
        self.graphics.drawMap(self.map)

class Game:

    def __init__(self):
        core = Core(1280, 720, 7)
