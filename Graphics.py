import pygame
import numpy

class Graphics:

    def __init__(self, width, height, blocksize, pipesize):
        '''Initialize Graphics object'''
        self.resolution = width, height
        self.blocksize = blocksize
        self.pipesize = pipesize
        pygame.init()
        screen = pygame.display.set_mode(self.resolution)
        screen.fill((255,255,255))
        pygame.display.flip()

    def clickHandler(self):
        ''' Click handeler '''
        while True:
            event = pygame.event.wait()

            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                self.rotateBlock(pos[0] // self.blocksize // self.pipesize, pos[1] // self.blocksize // self.pipesize)

            if event.type == pygame.QUIT:
                 break

    def drawPipe(self, gridX, gridY, pipe):
        '''
        This will draw a pipe in to the gird
        '''
        colorShape = pipe
        for i in range(len(colorShape)):
            for j in range(len(colorShape[i])):
                self.drawBlock(gridX + j, gridY + i, colorShape[j][i])
        pygame.display.flip()

    def drawBlock(self, gridX, gridY, color=(0,0,0)):
        '''
        This will draw a block in grid
        '''
        posX = gridX * self.blocksize
        posY = gridY * self.blocksize
        rect = pygame.Rect((posX, posY),(self.blocksize,self.blocksize))
        pygame.draw.rect(pygame.display.get_surface(), color, rect)

    def drawMap(self, pipeMap):
        ''' Draws map Fro 4dim array'''
        for y in range(len(pipeMap)):
            for x in range(len(pipeMap[y])):
                self.drawPipe(x * self.pipesize, y * self.pipesize, pipeMap[y][x])


    def getAt(self, posX, posY):
        '''get color on the grid'''
        px = posX * self.blocksize
        py = posY * self.blocksize
        return pygame.display.get_surface().get_at((px, py))

    def rotateBlock(self, gridX, gridY):
        '''Rotate block to left using numpy'''
        px =  gridX * self.pipesize
        py =  gridY * self.pipesize
        ij = []
        for n in range(self.pipesize):
            tmp = []
            for m in range(self.pipesize):
                tmp.append(self.getAt(px + n , py + m))
            ij.append(tmp)
        ij = numpy.rot90(ij)

        for i in range(self.pipesize):
            for j in range(self.pipesize):
                self.drawBlock(px + i, py + j, ij[i][j])

        pygame.display.flip()
